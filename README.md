# Seja bem vindo ao Debian

O mundo GNU é fascinante e nos oferece um amplo leque de possíveis direções a seguir. Ocorre que, enquanto todas estas possibilidades seja o que torna a jornada tão interessante, sabemos que este é exatamente o que torna o início tão difícil.

Pensando nisso, criamos este pequeno guia para te ajudar com os primeiros passos.

Esta é a sua página inicial, nela você encontrará apenas o essencial para não se sentir completamente perdido em seu sistema e nas conversas com os membros desta viva comunidade. Ao final, sugerimos algumas fontes de pesquisa, mas tentamos reunir as dúvidas e problemas mais comuns na nossa [wiki](https://gitlab.com/vbra/debian-bemvindo/-/wikis/home)

# O que é linux, GNU e Debian?

# O visual da minha instalação é diferente das que já vi, o que está acontecendo?

# Onde posso encontrar informações oficiais?

# Onde posso encontrar ajuda?

# Como posso obter ajudar de forma eficiente?

# Como posso instalar programas?

# Não encontro os programas que eu usava, o que posso fazer?

# Quero me aprofundar mais, quais os próximos passos?
